
val circeVersion = "0.9.3"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "uk.co.martingladdish",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "octopress-to-hugo",

    libraryDependencies += "org.slf4j" % "slf4j-api" % "1.8.0-beta2",
    libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.3.0-alpha4",

    libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0",
    libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.6.0",
    libraryDependencies += "io.circe" %% "circe-yaml" % "0.8.0",
    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser"
    ).map(_ % circeVersion),
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
  )
