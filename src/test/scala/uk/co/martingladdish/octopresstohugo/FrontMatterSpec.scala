package uk.co.martingladdish.octopresstohugo

import org.scalatest.{Matchers, WordSpec}
import uk.co.martingladdish.octopresstohugo.FrontMatter.Octopress

class FrontMatterSpec extends WordSpec with Matchers {

  "asHugo" should {

    val octo = Octopress("myLayout", "This is mY title", "2011-04-01 12:15", true, Seq("SSD", "performance testing", "Programming"))

    "pass the title through unchanged" in {
      octo.asHugo.title shouldBe octo.title
    }

    "re-format the date" in {
      octo.asHugo.date shouldBe "2011-04-01T12:15:00+01:00"
    }

    "build the alias" in {
      octo.asHugo.aliases shouldBe Seq("/blog/2011/04/01/this-is-my-title/")
    }
  }

  val octopress =
    """
      |layout: post
      |title: "HDD v SSD compiling benchmarks"
      |date: 2011-04-01 12:15
      |comments: true
      |categories: [SSD, performance testing, Programming]
    """.stripMargin

  "convert" should {
    "translate Octopress front matter to Hugo format" in {

      val result = FrontMatter.convert(octopress)

      val expected =
        """|title: HDD v SSD compiling benchmarks
           |date: '2011-04-01T12:15:00+01:00'
           |tags:
           |- SSD
           |- performance testing
           |- Programming
           |aliases:
           |- /blog/2011/04/01/hdd-v-ssd-compiling-benchmarks/
           |""".stripMargin

      result shouldBe expected
    }
  }

}
