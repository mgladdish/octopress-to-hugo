package uk.co.martingladdish.octopresstohugo

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.yaml.parser
import io.circe.yaml.syntax._

object FrontMatter {

  def convert(octopress: String): String = {
    val json = parser.parse(octopress).right.get
    val decoded = json.as[Octopress]
    decoded.right.get.asHugo.asJson.asYaml.spaces2
  }

  case class Octopress(layout: String, title: String, date: String, comments: Boolean, categories: Seq[String]) {
    val octoDT = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
    private val hugoDT: String = octoDT.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:00+01:00"))
    lazy val asHugo = Hugo(title, hugoDT, categories, Seq(s"/blog/${octoDT.getYear}/${octoDT.getMonth.formatted("%tm")}/${octoDT.getDayOfMonth.formatted("%02d")}/${title.replace(" ", "-").replace(":", "").toLowerCase(Locale.UK)}/"))
  }

  case class Hugo(title: String, date: String, tags: Seq[String], aliases: Seq[String])


}



