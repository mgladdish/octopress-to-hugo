package uk.co.martingladdish.octopresstohugo

import better.files._
import org.slf4j.LoggerFactory

/**
  * Assumes octopress site has at least the following layout:
  *
  * {{{
  * /contentDir
  * |-yyyy-MM-dd-title.markdown
  * /images
  * |-name.png
  * /downloads
  * |-/code
  *   |-external-code-sample.language-ext
  * }}}
  */
class Convert(sourceDir: String, targetDir: String) {

  private val logger = LoggerFactory.getLogger(getClass)


  def findOutstandingOctopressFiles() = {
    file"$sourceDir".list(f => f.extension.contains(".markdown") && !findAlreadyPortedFiles().exists(ported => f.name.contains(ported.nameWithoutExtension)))
  }

  def findAlreadyPortedFiles() = {
    file"$targetDir".list(f => f.extension.contains(".md") && !f.name.startsWith("_"))
  }

  def process(source: File) = {
    logger.debug(s"processing ${source.name}")
    val target = file"$targetDir/generated/${convertFilename(source.name)}"
    target.createFile()
    val assets = file"$targetDir/generated/${target.nameWithoutExtension}"

    val i = source.lineIterator
    for ((line, index) <- i.zipWithIndex) {
      (line, index) match {
        case ("---", 0) => target.appendLine("---" + "\n" + convertFrontMatter(i) + "\n" + "---")
        case (l, _) if l.startsWith("{% img") => {
          val (markdown, href) = convertImageTag(l)
          target.appendLine(markdown)
          assets.createDirectoryIfNotExists()
          file"$sourceDir/../images/$href".copyToDirectory(assets)
        }
        case (l, _) if l.startsWith("{% include_code") => target.appendLines(readCodeSourceFile(l):_*)
        case (l, _) if l.startsWith("{% codeblock") => target.appendLine(convertCodeBlockStart(l))
        case (l, _) if l.startsWith("{% endcodeblock %}") => target.appendLine("""{{< /highlight >}}""")
        case (l, _) => target.appendLine(l)
      }
    }
  }

  private def readCodeSourceFile(str: String): Seq[String] = {
    val sourceCodeFilename = """(?<=include_code )\w*\.\w*""".r
    val codeFile = file"$sourceDir/../downloads/code/${sourceCodeFilename.findFirstIn(str).get}"
    s"""{{< highlight ${codeFile.extension.fold("")(_.drop(1))}>}}""" +:
      codeFile.lines.toSeq :+
      """{{< /highlight >}}"""
  }

  private def convertCodeBlockStart(line: String): String = {
    val lang = """(?<=lang:)\w*""".r
    s"""{{< highlight ${lang.findFirstIn(line).get} >}}"""
  }

  private def convertFilename(octoFilename: String): String = {
    val name = """(?<=\d{4}-\d{2}-\d{2}-)[\w|-]*(?=\.markdown)""".r
    name.findFirstIn(octoFilename).get + ".md"
  }

  private def convertImageTag(line: String) = {
    val altText = """(?<=.png ).*(?=\s\%})""".r.findFirstIn(line).getOrElse("")
    val href = """\w*\.png""".r.findFirstIn(line).get

    (s"""![$altText]($href)""", href)
  }

  private def convertFrontMatter(i: Iterator[String]) = {
    var frontMatter = ""
    var line = i.next()

    while(line != "---" && i.hasNext) {
      frontMatter += line + "\n"
      line = i.next()
    }

    FrontMatter.convert(frontMatter)
  }



}
