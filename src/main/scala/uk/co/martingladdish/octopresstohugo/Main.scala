package uk.co.martingladdish.octopresstohugo

import org.slf4j.LoggerFactory

object Main {

  private val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {

    argsParser.parse(args, CommandLineArgs("", "")) match {
      case Some(cla) => {
        val converter = new Convert(cla.octopressPostsDirectory, cla.goHugoTargetDirectory)

        val alreadyPorted = converter.findAlreadyPortedFiles().map(_.name).toList
        logger.info(s"Already ported: ${alreadyPorted.mkString(", ")}")

        val stillToPort = converter.findOutstandingOctopressFiles().toList

        logger.info(s"Still to port: ${stillToPort.map(_.name).mkString(", ")}")

        stillToPort.foreach(converter.process)
      }
      case _ => System.exit(1)
    }
  }

  val argsParser = new scopt.OptionParser[CommandLineArgs]("octopress-to-hugo") {
    arg[String]("sourceDir")
      .action((s, cla) => cla.copy(octopressPostsDirectory = s))
        .text("The fully qualified path of Octopress's posts directory")

    arg[String]("targetDir")
      .action((s, cla) => cla.copy(goHugoTargetDirectory = s))
      .text("The fully qualified path of where the new Hugo files should be saved")
  }

}

case class CommandLineArgs(octopressPostsDirectory: String, goHugoTargetDirectory: String)